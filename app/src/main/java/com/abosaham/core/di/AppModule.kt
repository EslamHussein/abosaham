package com.abosaham.core.di

import android.content.Context
import com.abosaham.core.data.pref.AppPreference
import com.abosaham.core.data.pref.PREFERENCE_NAME
import com.abosaham.core.data.pref.PreferenceDataSource
import com.abosaham.core.data.pref.PreferenceDataSourceImpl
import com.abosaham.core.exception.DefaultErrorHandler
import com.abosaham.core.exception.ErrorHandler
import com.abosaham.core.executor.ExecutionThread
import com.abosaham.core.executor.UIThread
import com.abosaham.core.resource.AppResources
import com.abosaham.core.resource.repository.ResourcesRepository
import io.reactivex.disposables.CompositeDisposable
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.bind
import org.koin.dsl.module

val appModule = module {
    single { AppResources(context = androidApplication()) }
    single { ResourcesRepository(get()) }


    factory<ErrorHandler> {
        DefaultErrorHandler(get())
    } bind ErrorHandler::class


    factory<ExecutionThread> { UIThread() }
    factory { CompositeDisposable() }


    single { androidApplication().getSharedPreferences(PREFERENCE_NAME, Context.MODE_PRIVATE) }
    single { AppPreference(get()) }
    single { PreferenceDataSourceImpl(get(), get()) } bind PreferenceDataSource::class
}