package com.abosaham.core.domain

interface UseCase<P, R> {
    fun execute(param: P? = null): R
}