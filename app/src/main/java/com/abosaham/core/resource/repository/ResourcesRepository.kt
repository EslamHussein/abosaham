package com.abosaham.core.resource.repository

import com.abosaham.R
import com.abosaham.core.resource.AppResources

class ResourcesRepository(private val appResources: AppResources) {

    fun getNetworkExceptionMessage(): String = appResources.getString(R.string.no_internet_connection)

    fun getSocketTimeoutExceptionMessage(): String = appResources.getString(R.string.timeout_error_message)
    fun getGenericErrorMessage(): String = appResources.getString(R.string.something_went_wrong_please_try_again_later)

}