package com.abosaham.core.presentation.state

class Resource<T>(
    val status: ResourceState,
    val data: T? = null,
    val message: String? = null
)