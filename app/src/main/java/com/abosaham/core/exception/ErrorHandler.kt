package com.abosaham.core.exception

interface ErrorHandler {
    fun getErrorMessage(error: Throwable): String
}