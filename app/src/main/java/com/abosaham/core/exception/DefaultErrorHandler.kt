package com.abosaham.core.exception

import com.abosaham.R
import com.abosaham.core.data.remote.error.NetworkException
import com.abosaham.core.resource.AppResources
import java.io.IOException
import java.net.SocketException
import java.net.SocketTimeoutException
import java.net.UnknownHostException

internal class DefaultErrorHandler constructor(private val resourceManager: AppResources) : ErrorHandler {
    override fun getErrorMessage(error: Throwable): String {

        return when (error) {
            is IOException, is UnknownHostException, is SocketException
            -> resourceManager.getString(R.string.no_internet_connection)
            is SocketTimeoutException -> {
                resourceManager.getString(R.string.timeout_error_message)
            }
            is NetworkException -> {
                error.error
                    ?: resourceManager.getString(R.string.something_went_wrong_please_try_again_later)
            }
            is EmailNotValidException -> {
                resourceManager.getString(R.string.email_not_valid)
            }
            is NameNotValidException -> {
                resourceManager.getString(R.string.name_not_valid)
            }
            is PasswordNotValidException -> {
                resourceManager.getString(R.string.password_not_valid)
            }
            else -> resourceManager.getString(R.string.unknown_error)
        }
    }


}