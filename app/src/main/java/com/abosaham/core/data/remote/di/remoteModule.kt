package com.abosaham.core.data.remote.di

import com.abosaham.BuildConfig
import com.abosaham.core.data.remote.error.ErrorMappingInterceptor
import com.abosaham.core.data.remote.service.CloudConfig
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val remoteModule = module {
    single { HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY) }
    single { ErrorMappingInterceptor(get(), get()) }
    single {
        val builder = OkHttpClient.Builder()
        builder.addInterceptor(get<ErrorMappingInterceptor>())
            .connectTimeout(CloudConfig.CONNECT_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(CloudConfig.READ_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(CloudConfig.WRITE_TIMEOUT, TimeUnit.SECONDS)
        if (BuildConfig.DEBUG) {
            builder.addInterceptor(get<HttpLoggingInterceptor>())
        }
        builder.build()
    }
    single { GsonBuilder().create() }
    single {
        Retrofit.Builder()
            .baseUrl(CloudConfig.BASE_URL)
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create(get()))
            .client(get())
            .build()
    }
}