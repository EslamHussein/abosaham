package com.abosaham.core.data.remote.error

import com.abosaham.core.resource.repository.ResourcesRepository
import com.google.gson.Gson
import com.google.gson.JsonSyntaxException
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody
import java.io.IOException
import java.net.SocketTimeoutException
import java.nio.charset.Charset

const val successStatus = 1
const val failureStatus = 0

class ErrorMappingInterceptor(
    private val gson: Gson,
    private val resourcesRepository: ResourcesRepository
) : Interceptor {

    private val keyJson = "json"

    override fun intercept(chain: Interceptor.Chain): Response {

        val request = chain.request()
        val response: Response

        try {
            response = chain.proceed(request)
        } catch (e: IOException) {
            if (e is SocketTimeoutException) {
                throw NetworkException(failureStatus, resourcesRepository.getSocketTimeoutExceptionMessage())
            } else {
                throw NetworkException(failureStatus, resourcesRepository.getNetworkExceptionMessage())
            }
        }

        val body = response.body()!!
        // Only intercept JSON type responses and ignore the rest.
        val source = body.source()
        source.request(java.lang.Long.MAX_VALUE) // Buffer the entire body.
        val buffer = source.buffer()
        val charset = body.contentType()!!.charset(Charset.forName("UTF-8"))!!
        // Clone the existing buffer is they can only read once so we still want to pass the original one to the chain.
        val responseBody = buffer.clone().readString(charset)
        try {

            val apiException = gson.fromJson(responseBody, NetworkException::class.java)

            if (response.isSuccessful.not().or(apiException.status != successStatus)) {
                if (apiException.error.isNullOrEmpty()) {
                    if (apiException.message.isNullOrBlank().not()) {
                        apiException.error = apiException.message!!
                    } else {
                        apiException.error = resourcesRepository.getGenericErrorMessage()
                    }
                }

                throw apiException
            }

        } catch (e: JsonSyntaxException) {
            throw NetworkException(failureStatus, resourcesRepository.getGenericErrorMessage())
        }



        return response
    }

    private fun isJsonTypeResponse(body: ResponseBody?): Boolean {
        return body?.contentType() != null && body.contentType()!!.subtype().toLowerCase() == keyJson
    }

}