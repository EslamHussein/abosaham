package com.abosaham.core.data.pref

import android.content.SharedPreferences


const val PREFERENCE_NAME = "AboSaham"
const val PROPERTY_USER = "com.abosaham.user"
const val PROPERTY_CURRENT_LANGUAGE = "com.abosaham.current.language"

class AppPreference(private val preferences: SharedPreferences) {

    fun getString(key: String, defaultValue: String): String = preferences.getString(key, defaultValue)!!

    fun putString(key: String, value: String) {
        preferences.edit().putString(key, value).apply()
    }

    fun getLong(key: String, defaultValue: Long) = preferences.getLong(key, defaultValue)

    fun putLong(key: String, value: Long) {
        preferences.edit().putLong(key, value).apply()
    }

    fun getBoolean(key: String, defaultValue: Boolean) = preferences.getBoolean(key, defaultValue)

    fun putBoolean(key: String, value: Boolean) {
        preferences.edit().putBoolean(key, value).apply()
    }

    fun putFloat(key: String, value: Float) {
        preferences.edit().putFloat(key, value).apply()
    }

    fun putInt(key: String, value: Int) = preferences.edit().putInt(key, value).apply()

    fun getInt(key: String, defaultValue: Int) = preferences.getInt(key, defaultValue)

}