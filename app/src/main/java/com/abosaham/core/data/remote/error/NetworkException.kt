package com.abosaham.core.data.remote.error

import com.google.gson.annotations.SerializedName

class NetworkException(@SerializedName("Status") val status: Int?, @SerializedName("Errors") var error: String? = null) :
    Exception()