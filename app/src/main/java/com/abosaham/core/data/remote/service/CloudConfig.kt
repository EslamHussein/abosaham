package com.abosaham.core.data.remote.service

object CloudConfig {
    const val BASE_URL = "http://abosahmnews.com/"
    const val CONNECT_TIMEOUT: Long = 30
    const val READ_TIMEOUT: Long = 30
    const val WRITE_TIMEOUT: Long = 30

    const val BASE_IMAGE_URL = "http://abosahmnews.com/uploads/"

}