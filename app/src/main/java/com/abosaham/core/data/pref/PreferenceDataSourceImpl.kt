package com.abosaham.core.data.pref

import com.abosaham.authentication.remote.dto.User
import com.abosaham.core.AR
import com.google.gson.Gson

class PreferenceDataSourceImpl(private val appPreference: AppPreference, private val gson: Gson) :
    PreferenceDataSource {
    override fun getCurrentLanguage(): String {
        return appPreference.getString(PROPERTY_CURRENT_LANGUAGE, AR)
    }

    override fun setCurrentLanguage(lang: String) {
        return appPreference.putString(PROPERTY_CURRENT_LANGUAGE, lang)
    }

    override fun setUser(user: User) {
        appPreference.putString(PROPERTY_USER, gson.toJson(user))
    }

    override fun getUser(): User? {
        val userJson = appPreference.getString(PROPERTY_USER, "")
        if (userJson.isEmpty())
            return null
        return gson.fromJson(userJson, User::class.java)
    }
}