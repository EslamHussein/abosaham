package com.abosaham.core.data.pref

import com.abosaham.authentication.remote.dto.User

interface PreferenceDataSource {
    fun setUser(user: User)
    fun getUser(): User?
    fun getCurrentLanguage(): String
    fun setCurrentLanguage(lang: String)
}