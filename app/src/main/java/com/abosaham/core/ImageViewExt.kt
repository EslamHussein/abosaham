package com.abosaham.core

import android.widget.ImageView
import com.abosaham.core.data.remote.service.CloudConfig
import com.bumptech.glide.Glide

fun ImageView.loadUrl(url: String? = null) {
    url?.let {
        Glide.with(context).load(CloudConfig.BASE_IMAGE_URL + it).into(this)
    }
}