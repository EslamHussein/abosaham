package com.abosaham.core.mapper

interface Mapper<F,T> {
    fun map(from:F):T
}