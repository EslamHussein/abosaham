package com.abosaham

import android.app.Application
import android.content.Context
import android.content.res.Configuration
import com.abosaham.authentication.di.authenticationModule
import com.abosaham.core.data.remote.di.remoteModule
import com.abosaham.core.di.appModule
import com.abosaham.news.di.newsModule
import com.akexorcist.localizationactivity.core.LocalizationApplicationDelegate
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin


class AboSaham : Application() {

    var localizationDelegate = LocalizationApplicationDelegate(this)


    override fun onCreate() {
        super.onCreate()
        instance = this
        startKoin {
            androidLogger()
            androidContext(this@AboSaham)
            modules(listOf(remoteModule, appModule, authenticationModule, newsModule))
        }
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(localizationDelegate.attachBaseContext(base))
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        localizationDelegate.onConfigurationChanged(this)
    }

    override fun getApplicationContext(): Context {
        return localizationDelegate.getApplicationContext(super.getApplicationContext())
    }

    companion object {
        private var instance: AboSaham? = null
        fun get(): AboSaham {
            if (instance == null)
                throw IllegalStateException("Something went horribly wrong!!, no application attached!")
            return instance as AboSaham
        }
    }

}