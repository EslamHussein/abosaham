package com.abosaham.authentication.ui

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import com.abosaham.R
import com.abosaham.authentication.presentation.viewmodel.SignInViewModel
import com.abosaham.core.hideSoftKeyboard
import com.abosaham.core.presentation.state.ResourceState
import com.abosaham.core.showSnackBar
import com.abosaham.news.ui.activities.HomeActivity
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.facebook.CallbackManager
import com.facebook.FacebookCallback
import com.facebook.FacebookException
import com.facebook.GraphRequest
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import kotlinx.android.synthetic.main.activity_sign_in.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class SignInActivity : LocalizationActivity(), FacebookCallback<LoginResult> {

    private val rcSignInRequestCode: Int = 1001

    private val viewModel: SignInViewModel by viewModel()
    private var progressDialog: ProgressDialog? = null

    private val gso: GoogleSignInOptions by lazy {
        GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.server_client_id))
                .requestEmail()
                .build()
    }

    private val mGoogleSignInClient: GoogleSignInClient by lazy {
        GoogleSignIn.getClient(this, gso)
    }

    private val callbackManager: CallbackManager by lazy {
        CallbackManager.Factory.create()
    }

    companion object {
        fun start(from: Context) =
                from.startActivity(Intent(from, SignInActivity::class.java))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_in)
        progressDialog = ProgressDialog(this)
        progressDialog?.apply {
            setMessage(getString(R.string.loading))
            setCancelable(false)
            setCanceledOnTouchOutside(false)
        }

        LoginManager.getInstance().registerCallback(callbackManager, this)

        viewModel.getSignIn().observe(this, Observer {
            Log.d(this.javaClass.name, "${it.status}")

            when (it.status) {
                ResourceState.SUCCESS -> {
                    HomeActivity.start(this@SignInActivity)
                    progressDialog?.dismiss()
                }
                ResourceState.ERROR -> {
                    progressDialog?.dismiss()
                    window.decorView.rootView.showSnackBar(msg = it.message!!)
                }
                ResourceState.LOADING -> {
                    progressDialog?.show()
                }
            }
        })

        viewModel.getSignInSocial().observe(this, Observer {
            Log.d(this.javaClass.name, "${it.status}")

            when (it.status) {
                ResourceState.SUCCESS -> {
                    mGoogleSignInClient.signOut()
                    mGoogleSignInClient.revokeAccess()
                    HomeActivity.start(this@SignInActivity)
                    progressDialog?.dismiss()
                }
                ResourceState.ERROR -> {
                    progressDialog?.dismiss()
                    window.decorView.rootView.showSnackBar(msg = it.message!!)
                }
                ResourceState.LOADING -> {
                    progressDialog?.show()
                }
            }
        })

        ivSignInWithGoogle.setOnClickListener {
            val account = GoogleSignIn.getLastSignedInAccount(this)
            if (account != null) {
                signInWithGoogleAccount(account)
            } else {
                val signInIntent = mGoogleSignInClient.signInIntent
                startActivityForResult(signInIntent, rcSignInRequestCode)
            }
        }

        ivLoginWithFacebook.setOnClickListener {
            LoginManager.getInstance().logInWithReadPermissions(this, listOf("public_profile", "email"))
        }

        btnLogIn.setOnClickListener {
            viewModel.signIn(etEmail.text.toString(), etPassword.text.toString())
            hideSoftKeyboard()
        }

        btnSignUp.setOnClickListener {
            SignUpActivity.start(this@SignInActivity)
            finish()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == rcSignInRequestCode) {
            handleSignInWithGoogleResult(GoogleSignIn.getSignedInAccountFromIntent(data))
        }
    }

    private fun handleSignInWithGoogleResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)!!

            signInWithGoogleAccount(account)

            // Signed in successfully, show authenticated UI.
            Log.i(
                    this.javaClass.name,
                    "Name: ${account.givenName}, Email: ${account.email}, Id: ${account.id}, Photo URL: ${account.photoUrl.toString()}"
            )
        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            window.decorView.rootView.showSnackBar(msg = e.message!!)
            Log.w(this.javaClass.name, "signInResult:failed code=" + e.statusCode)
        }

    }

    private fun signInWithGoogleAccount(account: GoogleSignInAccount) {
        val name = account.displayName
        val email = account.email
        val id = account.id
        val photoUrl = if (account.photoUrl != null) account.photoUrl.toString() else ""

        if (!name.isNullOrBlank() && !email.isNullOrBlank() && !id.isNullOrBlank()) {
            viewModel.signInSocial(name, email, id, "google", photoUrl)
        } else {
            window.decorView.rootView.showSnackBar(getString(R.string.something_went_wrong_please_try_again_later))
        }
    }

    override fun onSuccess(result: LoginResult?) {
        result?.let {
            val request = GraphRequest.newMeRequest(
                    it.accessToken
            ) { jsonObject, response ->
                Log.i(this@SignInActivity.javaClass.name, response.rawResponse)
                Log.i(this@SignInActivity.javaClass.name, jsonObject.toString())

                if (jsonObject.has("name") &&
                        jsonObject.has("email") &&
                        jsonObject.has("id")
                ) {
                    val name = jsonObject.getString("name")
                    val email = jsonObject.getString("email")
                    val id = jsonObject.getString("id")
                    val photoUrl = if (jsonObject.has("picture")) {
                        val pictureObject = jsonObject.getJSONObject("picture")
                        if (pictureObject.has("data")) {
                            val dataObject = pictureObject.getJSONObject("data")
                            if (dataObject.has("url")) {
                                dataObject.getString("url")
                            } else {
                                ""
                            }
                        } else {
                            ""
                        }
                    } else {
                        ""
                    }
                    Log.i(
                            this.javaClass.name,
                            "Name: $name, Email: $email, Id: $id, Photo URL: $photoUrl"
                    )
                    if (!name.isNullOrBlank() && !email.isNullOrBlank() && !id.isNullOrBlank()) {
                        viewModel.signInSocial(name, email, id, "facebook", photoUrl)
                    } else {
                        window.decorView.rootView.showSnackBar(getString(R.string.something_went_wrong_please_try_again_later))
                    }
                }
            }
            request.parameters.apply {
                putString("fields", "id,name,email,picture.width(200)")
            }
            request.executeAsync()
        }
    }

    override fun onCancel() {
        window.decorView.rootView.showSnackBar(getString(R.string.user_cancelled))
    }

    override fun onError(error: FacebookException?) {
        window.decorView.rootView.showSnackBar(
                error?.message ?: getString(R.string.something_went_wrong_please_try_again_later)
        )
    }

}
