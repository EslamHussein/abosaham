package com.abosaham.authentication.ui

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.abosaham.R
import com.abosaham.authentication.presentation.viewmodel.ProfileViewModel
import com.abosaham.authentication.remote.dto.User
import com.abosaham.core.AR
import com.abosaham.core.EN
import com.abosaham.core.presentation.state.ResourceState
import com.abosaham.core.showSnackBar
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.input.input
import com.afollestad.materialdialogs.list.listItems
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.content_language.*
import kotlinx.android.synthetic.main.content_user_data.*
import org.koin.android.ext.android.inject


class ProfileActivity : LocalizationActivity(), View.OnClickListener {


    private val viewModel: ProfileViewModel by inject()

    private var progressDialog: ProgressDialog? = null

    companion object {
        fun start(from: Context) =
            from.startActivity(Intent(from, ProfileActivity::class.java))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        setSupportActionBar(profileToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        progressDialog = ProgressDialog(this)
        progressDialog?.apply {
            setMessage(getString(R.string.loading))
            setCancelable(false)
            setCanceledOnTouchOutside(false)
        }


        viewModel.profile().observe(this, Observer {
            when (it.status) {
                ResourceState.LOADING -> {
                    progressDialog?.show()
                }
                ResourceState.SUCCESS -> {
                    bindProfile(it.data!!)
                    progressDialog?.dismiss()

                }
                ResourceState.ERROR -> {
                    progressDialog?.dismiss()
                    window.decorView.rootView.showSnackBar(msg = it.message!!)

                }
            }
        })

        viewModel.language().observe(this, Observer {
            when (it.status) {
                ResourceState.SUCCESS -> {
                    bindLanguage(it.data!!)
                }
            }
        })
        viewModel.getCurrentLanguage()
        viewModel.getProfile()
        nameTextView.setOnClickListener(this)
        nameTitleTextView.setOnClickListener(this)

        emailTitleTextView.setOnClickListener(this)
        emailTextView.setOnClickListener(this)

        changePasswordTextView.setOnClickListener(this)

        languageInclude.setOnClickListener(this)


        mobileNumberTitleTextView.setOnClickListener(null)
        mobileNumberTextView.setOnClickListener(null)


    }

    private fun bindProfile(user: User) {
        nameTextView.text = user.name
        mobileNumberTextView.text = user.phone ?: ""
        emailTextView.text = user.email
    }

    private fun bindLanguage(lang: String) {
        languageTextView.text = getString(if (lang == AR) R.string.arabic else R.string.english)
    }


    override fun onClick(v: View?) {


        when (v?.id) {
            R.id.nameTitleTextView, R.id.nameTextView -> {
                MaterialDialog(this).show {
                    input(getString(R.string.enter_your_name)) { materialDialog, text ->
                        viewModel.updateName(text.toString())
                    }
                    title(R.string.update_your_name)
                    positiveButton(R.string.submit)
                }
            }

            R.id.changePasswordTextView -> {
                MaterialDialog(this).show {
                    input(getString(R.string.enter_your_password)) { materialDialog, text ->

                        viewModel.updatePassword(text.toString())
                    }
                    title(R.string.change_password)
                    positiveButton(R.string.submit)
                }

            }
            R.id.languageInclude -> {

                MaterialDialog(this).show {
                    title(R.string.language)
                    listItems(R.array.languages) { dialog, index, text ->
                        val newLanguage = if (index == 0) AR else EN
                        viewModel.updateLanguage(newLanguage)
                        setLanguage(newLanguage)
                        restartApp()
                    }
                }
            }
            else -> {

            }
        }

    }

    private fun restartApp() {
        val i = baseContext.packageManager.getLaunchIntentForPackage(baseContext.packageName)
        i!!.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(i)
        finish()
    }
}
