package com.abosaham.authentication.ui

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.Observer
import com.abosaham.R
import com.abosaham.authentication.presentation.viewmodel.SignUpViewModel
import com.abosaham.core.hideSoftKeyboard
import com.abosaham.core.presentation.state.ResourceState
import com.abosaham.core.showSnackBar
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import kotlinx.android.synthetic.main.activity_sign_up.*
import org.koin.android.ext.android.inject

class SignUpActivity : LocalizationActivity() {


    private val viewModel: SignUpViewModel by inject()

    private var progressDialog: ProgressDialog? = null


    companion object {
        fun start(from: Context) =
            from.startActivity(Intent(from, SignUpActivity::class.java))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign_up)

        progressDialog = ProgressDialog(this)
        progressDialog?.apply {
            setMessage(getString(R.string.loading))
            setCancelable(false)
            setCanceledOnTouchOutside(false)
        }
        viewModel.getSignUp().observe(this, Observer {
            Log.d(this.javaClass.name, "${it.status}")

            when (it.status) {
                ResourceState.SUCCESS -> {
                    progressDialog?.dismiss()
                    SignInActivity.start(this@SignUpActivity)
                    finish()
                }
                ResourceState.ERROR -> {
                    window.decorView.rootView.showSnackBar(msg = it.message!!)
                    progressDialog?.dismiss()
                }
                ResourceState.LOADING -> {
                    progressDialog?.show()
                }
            }
        })

        btnSignUp.setOnClickListener {
            viewModel.signUp(
                    etFullName.text.toString(),
                    etEmail.text.toString(),
                    etPassword.text.toString(), etConfirmPassword.text.toString()
            )
            hideSoftKeyboard()
        }

        btnLogIn.setOnClickListener {
            SignInActivity.start(this@SignUpActivity)
            finish()
        }
    }
}
