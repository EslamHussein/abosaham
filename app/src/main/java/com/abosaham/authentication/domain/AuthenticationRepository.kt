package com.abosaham.authentication.domain

import com.abosaham.authentication.remote.dto.ForgetPasswordResponse
import com.abosaham.authentication.remote.dto.User
import io.reactivex.Completable
import io.reactivex.Single

interface AuthenticationRepository {
    fun signUp(params: SignUp.Params): Completable
    fun signIn(params: SignIn.Params): Single<User>
    fun forgetPassword(params: ForgetPassword.Params): Single<ForgetPasswordResponse>
    fun signInSocial(params: SignInSocial.Params): Single<User>
    fun getProfile(): User?
    fun getCurrentLanguage(): String
    fun updateCurrentLanguage(newLanguage: String)
    fun updatePassword(params: UpdatePassword.Params): Single<User>
    fun updateName(params: UpdateName.Params): Single<User>

}