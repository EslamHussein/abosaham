package com.abosaham.authentication.domain

import com.abosaham.core.domain.UseCase
import com.abosaham.core.exception.EmailNotValidException
import com.abosaham.core.exception.NameNotValidException
import com.abosaham.core.exception.PasswordNotValidException
import com.abosaham.core.isEmailValid
import io.reactivex.Completable

class SignUp(private val authenticationRepository: AuthenticationRepository) :
    UseCase<SignUp.Params, Completable> {
    override fun execute(param: Params?): Completable {
        if (param == null) throw IllegalArgumentException("${Params::class.java} should be not null.")

        if (param.name.isEmpty())
            return Completable.error(NameNotValidException())

        if (!param.email.isEmailValid())
            return Completable.error(EmailNotValidException())

        if (param.confirmPassword.isEmpty() || param.password.isEmpty() || param.password != param.confirmPassword)
            return Completable.error(PasswordNotValidException())

        return authenticationRepository.signUp(param)
    }

    class Params(val name: String, val email: String, val password: String, val confirmPassword: String) {
        companion object {
            fun create(name: String, email: String, password: String, confirmPassword: String): Params {
                return Params(name, email, password, confirmPassword)
            }
        }
    }
}