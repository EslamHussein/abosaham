package com.abosaham.authentication.domain

import com.abosaham.authentication.remote.dto.User
import com.abosaham.core.domain.UseCase
import io.reactivex.Single

class UpdateName(private val authenticationRepository: AuthenticationRepository) :
    UseCase<UpdateName.Params, Single<User>> {
    override fun execute(param: Params?): Single<User> {
        if (param == null) throw IllegalArgumentException("${Params::class.java} should be not null.")
        return authenticationRepository.updateName(param)
    }

    class Params(val newName: String) {
        companion object {
            fun create(newPassword: String): Params {
                return Params(newPassword)
            }
        }
    }
}