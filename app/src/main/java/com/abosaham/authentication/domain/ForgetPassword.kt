package com.abosaham.authentication.domain

import com.abosaham.authentication.remote.dto.ForgetPasswordResponse
import com.abosaham.core.domain.UseCase
import io.reactivex.Single

class ForgetPassword(private val authenticationRepository: AuthenticationRepository) :
    UseCase<ForgetPassword.Params, Single<ForgetPasswordResponse>> {
    override fun execute(param: Params?): Single<ForgetPasswordResponse> {
        if (param == null) throw IllegalArgumentException("${Params::class.java} should be not null.")
        return authenticationRepository.forgetPassword(param)
    }

    class Params(val email: String) {
        companion object {
            fun create(email: String): Params {
                return Params(email)
            }
        }
    }
}