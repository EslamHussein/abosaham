package com.abosaham.authentication.domain

import com.abosaham.authentication.remote.dto.User
import com.abosaham.core.domain.UseCase
import com.abosaham.core.exception.EmailNotValidException
import com.abosaham.core.exception.PasswordNotValidException
import com.abosaham.core.isEmailValid
import io.reactivex.Single

class SignIn(private val authenticationRepository: AuthenticationRepository) :
    UseCase<SignIn.Params, Single<User>> {
    override fun execute(param: Params?): Single<User> {
        if (param == null) throw IllegalArgumentException("${Params::class.java} should be not null.")
        if (!param.email.isEmailValid())
            return Single.error(EmailNotValidException())

        if (param.password.isEmpty())
            return Single.error(PasswordNotValidException())
        return authenticationRepository.signIn(param)
    }

    class Params(val email: String, val password: String) {
        companion object {
            fun create(email: String, password: String): Params {
                return Params(email, password)
            }
        }
    }

}