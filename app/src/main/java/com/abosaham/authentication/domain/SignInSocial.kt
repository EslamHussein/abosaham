package com.abosaham.authentication.domain

import com.abosaham.authentication.remote.dto.User
import com.abosaham.core.domain.UseCase
import io.reactivex.Single

class SignInSocial(private val authenticationRepository: AuthenticationRepository) :
    UseCase<SignInSocial.Params, Single<User>> {
    override fun execute(param: Params?): Single<User> {
        if (param == null) throw IllegalArgumentException("${Params::class.java} should be not null.")
        return authenticationRepository.signInSocial(param)
    }

    class Params(val name: String, val email: String, val userID: String, val provider: String, val photoURL: String) {
        companion object {
            fun create(name: String, email: String, userID: String, provider: String, photoURL: String): Params {
                return Params(name, email, userID, provider, photoURL)
            }
        }
    }
}