package com.abosaham.authentication.domain

import com.abosaham.authentication.remote.dto.User
import com.abosaham.core.domain.UseCase

class GetCurrentLanguage(private val authenticationRepository: AuthenticationRepository) :
    UseCase<Unit, String> {
    override fun execute(param: Unit?): String {
        return authenticationRepository.getCurrentLanguage()
    }
}