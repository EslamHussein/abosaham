package com.abosaham.authentication.domain

import com.abosaham.core.domain.UseCase

class UpdateCurrentLanguage(private val authenticationRepository: AuthenticationRepository) :
    UseCase<UpdateCurrentLanguage.Params, Unit> {
    override fun execute(param: Params?) {
        if (param == null) throw IllegalArgumentException("${Params::class.java} should be not null.")
        return authenticationRepository.updateCurrentLanguage(param.newLanguage)
    }


    class Params(val newLanguage: String) {
        companion object {
            fun create(newLanguage: String): Params {
                return Params(newLanguage)
            }
        }
    }
}