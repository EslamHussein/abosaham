package com.abosaham.authentication.domain

import com.abosaham.authentication.remote.dto.User
import com.abosaham.core.domain.UseCase

class GetProfile(private val authenticationRepository: AuthenticationRepository) :
    UseCase<Unit, User> {
    override fun execute(param: Unit?): User {
        return authenticationRepository.getProfile()!!
    }
}