package com.abosaham.authentication.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.abosaham.authentication.domain.*
import com.abosaham.authentication.remote.dto.User
import com.abosaham.core.exception.ErrorHandler
import com.abosaham.core.executor.ExecutionThread
import com.abosaham.core.presentation.state.Resource
import com.abosaham.core.presentation.state.ResourceState
import io.reactivex.disposables.CompositeDisposable

class ProfileViewModel(
    private val errorHandler: ErrorHandler, private val executionThread: ExecutionThread,
    private val compositeDisposable: CompositeDisposable,
    private val getProfileUseCase: GetProfile,
    private val getCurrentLanguageUseCase: GetCurrentLanguage,
    private val updateCurrentLanguageUseCase: UpdateCurrentLanguage,
    private val updatePasswordUseCase: UpdatePassword,
    private val updateNameUseCase: UpdateName
) : ViewModel() {
    private val profileLiveData: MutableLiveData<Resource<User>> by lazy { MutableLiveData<Resource<User>>() }
    private val languageLiveData: MutableLiveData<Resource<String>> by lazy { MutableLiveData<Resource<String>>() }


    fun profile(): LiveData<Resource<User>> {
        return profileLiveData
    }

    fun getProfile() {
        profileLiveData.postValue(Resource(ResourceState.SUCCESS, getProfileUseCase.execute(), null))
    }


    fun language(): LiveData<Resource<String>> {
        return languageLiveData
    }

    fun getCurrentLanguage() {
        languageLiveData.postValue(Resource(ResourceState.SUCCESS, getCurrentLanguageUseCase.execute(), null))
    }

    fun updateLanguage(lang: String) {
        updateCurrentLanguageUseCase.execute(UpdateCurrentLanguage.Params.create(lang))
    }


    fun updatePassword(newPassword:String){
        compositeDisposable.add(
            updatePasswordUseCase.execute(UpdatePassword.Params.create(newPassword))
                .subscribeOn(executionThread.subscribeScheduler)
                .observeOn(executionThread.observerScheduler)
                .doOnSubscribe {
                    profileLiveData.postValue(Resource(ResourceState.LOADING, null, null))
                }
                .subscribe({ response ->
                    profileLiveData.postValue(Resource(ResourceState.SUCCESS, response, null))
                }, { throwable ->
                    profileLiveData.postValue(
                        Resource(
                            ResourceState.ERROR,
                            null,
                            errorHandler.getErrorMessage(throwable)
                        )
                    )
                })
        )
    }

    fun updateName(newName:String){
        compositeDisposable.add(
            updateNameUseCase.execute(UpdateName.Params.create(newName))
                .subscribeOn(executionThread.subscribeScheduler)
                .observeOn(executionThread.observerScheduler)
                .doOnSubscribe {
                    profileLiveData.postValue(Resource(ResourceState.LOADING, null, null))
                }
                .subscribe({ response ->
                    profileLiveData.postValue(Resource(ResourceState.SUCCESS, response, null))
                }, { throwable ->
                    profileLiveData.postValue(
                        Resource(
                            ResourceState.ERROR,
                            null,
                            errorHandler.getErrorMessage(throwable)
                        )
                    )
                })
        )
    }


    override fun onCleared() {
        if (!compositeDisposable.isDisposed) compositeDisposable.dispose()
        super.onCleared()
    }


}
