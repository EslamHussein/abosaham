package com.abosaham.authentication.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.abosaham.authentication.domain.SignUp
import com.abosaham.authentication.remote.dto.SignUpResponse
import com.abosaham.core.exception.ErrorHandler
import com.abosaham.core.executor.ExecutionThread
import com.abosaham.core.presentation.state.Resource
import com.abosaham.core.presentation.state.ResourceState
import io.reactivex.disposables.CompositeDisposable

class SignUpViewModel(
    private val errorHandler: ErrorHandler, private val executionThread: ExecutionThread,
    private val compositeDisposable: CompositeDisposable,
    private val signUpUseCase: SignUp
) : ViewModel() {
    private val signUpLiveData: MutableLiveData<Resource<SignUpResponse>> by lazy { MutableLiveData<Resource<SignUpResponse>>() }


    fun getSignUp(): LiveData<Resource<SignUpResponse>> {
        return signUpLiveData
    }

    fun signUp(name: String, email: String, password: String, confirmPassword: String) {
        compositeDisposable.add(
            signUpUseCase.execute(
                SignUp.Params.create(name, email, password, confirmPassword)
            )
                .subscribeOn(executionThread.subscribeScheduler)
                .observeOn(executionThread.observerScheduler)
                .doOnSubscribe {
                    signUpLiveData.postValue(Resource(ResourceState.LOADING, null, null))
                }
                .subscribe({
                    signUpLiveData.postValue(Resource(ResourceState.SUCCESS, null, null))
                }, { throwable ->
                    signUpLiveData.postValue(
                        Resource(
                            ResourceState.ERROR,
                            null,
                            errorHandler.getErrorMessage(throwable)
                        )
                    )
                })
        )

    }

    override fun onCleared() {
        if (!compositeDisposable.isDisposed) compositeDisposable.dispose()
        super.onCleared()
    }
}
