package com.abosaham.authentication.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.abosaham.authentication.domain.SignIn
import com.abosaham.authentication.domain.SignInSocial
import com.abosaham.authentication.remote.dto.User
import com.abosaham.core.exception.ErrorHandler
import com.abosaham.core.executor.ExecutionThread
import com.abosaham.core.presentation.state.Resource
import com.abosaham.core.presentation.state.ResourceState
import io.reactivex.disposables.CompositeDisposable

class SignInViewModel(
    private val errorHandler: ErrorHandler, private val executionThread: ExecutionThread,
    private val compositeDisposable: CompositeDisposable,
    private val signInUseCase: SignIn,
    private val signInSocialUseCase: SignInSocial
) : ViewModel() {
    private val signInLiveData: MutableLiveData<Resource<User>> by lazy { MutableLiveData<Resource<User>>() }
    private val signInSocialLiveData: MutableLiveData<Resource<User>> by lazy { MutableLiveData<Resource<User>>() }


    fun getSignIn(): LiveData<Resource<User>> {
        return signInLiveData
    }

    fun getSignInSocial(): LiveData<Resource<User>> {
        return signInSocialLiveData
    }

    fun signIn(email: String, password: String) {
        compositeDisposable.add(
            signInUseCase.execute(
                SignIn.Params.create(email, password)
            )
                .subscribeOn(executionThread.subscribeScheduler)
                .observeOn(executionThread.observerScheduler)
                .doOnSubscribe {
                    signInLiveData.postValue(Resource(ResourceState.LOADING, null, null))
                }
                .subscribe({ response ->
                    signInLiveData.postValue(Resource(ResourceState.SUCCESS, response, null))
                }, { throwable ->
                    signInLiveData.postValue(
                        Resource(
                            ResourceState.ERROR,
                            null,
                            errorHandler.getErrorMessage(throwable)
                        )
                    )
                })
        )

    }

    fun signInSocial(name: String, email: String, userID: String, provider: String, photoURL: String) {
        compositeDisposable.add(
            signInSocialUseCase.execute(
                SignInSocial.Params.create(name, email, userID, provider, photoURL)
            )
                .subscribeOn(executionThread.subscribeScheduler)
                .observeOn(executionThread.observerScheduler)
                .doOnSubscribe {
                    signInSocialLiveData.postValue(Resource(ResourceState.LOADING, null, null))
                }
                .subscribe({ response ->
                    signInSocialLiveData.postValue(Resource(ResourceState.SUCCESS, response, null))
                }, { throwable ->
                    signInSocialLiveData.postValue(
                        Resource(
                            ResourceState.ERROR,
                            null,
                            errorHandler.getErrorMessage(throwable)
                        )
                    )
                })
        )

    }

    override fun onCleared() {
        if (!compositeDisposable.isDisposed) compositeDisposable.dispose()
        super.onCleared()
    }
}
