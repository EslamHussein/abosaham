package com.abosaham.authentication.repository

import com.abosaham.authentication.domain.*
import com.abosaham.authentication.remote.AuthenticationRemoteDataSource
import com.abosaham.authentication.remote.dto.ForgetPasswordResponse
import com.abosaham.authentication.remote.dto.User
import com.abosaham.core.data.pref.PreferenceDataSource
import io.reactivex.Completable
import io.reactivex.Single

class AuthenticationRepositoryImpl(
    private val remoteDataSource: AuthenticationRemoteDataSource,
    private val preferenceDataSource: PreferenceDataSource
) :
    AuthenticationRepository {
    override fun updateName(params: UpdateName.Params): Single<User> {
        val user = preferenceDataSource.getUser()!!

        return remoteDataSource.updateName(user.email, user.token!!, params.newName).map {
            user.apply {
                name = it.name
            }
        }.doOnSuccess {
            preferenceDataSource.setUser(user)
        }
    }

    override fun updatePassword(params: UpdatePassword.Params): Single<User> {

        val user = preferenceDataSource.getUser()!!
        return remoteDataSource.updatePassword(user.email, user.token!!, params.newPassword).map {

            user.apply {
                token = it.password
            }
        }.doOnSuccess {
            preferenceDataSource.setUser(user)
        }

    }

    override fun updateCurrentLanguage(newLanguage: String) {
        preferenceDataSource.setCurrentLanguage(newLanguage)
    }

    override fun getCurrentLanguage(): String {
        return preferenceDataSource.getCurrentLanguage()
    }

    override fun getProfile(): User? {
        return preferenceDataSource.getUser()
    }

    override fun signUp(params: SignUp.Params): Completable {
        return remoteDataSource.signUp(params.name, params.email, params.password)
    }

    override fun signIn(params: SignIn.Params): Single<User> {
        return remoteDataSource.signIn(params.email, params.password).doOnSuccess {
            preferenceDataSource.setUser(it)
        }
    }

    override fun forgetPassword(params: ForgetPassword.Params): Single<ForgetPasswordResponse> {
        return remoteDataSource.forgetPassword(params.email)
    }

    override fun signInSocial(params: SignInSocial.Params): Single<User> {
        return remoteDataSource.signInSocial(params.name, params.email, params.userID, params.provider, params.photoURL)
            .doOnSuccess {
                preferenceDataSource.setUser(it)
            }
    }


}