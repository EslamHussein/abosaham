package com.abosaham.authentication.di

import com.abosaham.authentication.domain.*
import com.abosaham.authentication.presentation.viewmodel.ProfileViewModel
import com.abosaham.authentication.presentation.viewmodel.SignInViewModel
import com.abosaham.authentication.presentation.viewmodel.SignUpViewModel
import com.abosaham.authentication.remote.AuthenticationRemoteDataSource
import com.abosaham.authentication.remote.AuthenticationRemoteDataSourceImpl
import com.abosaham.authentication.remote.AuthenticationService
import com.abosaham.authentication.repository.AuthenticationRepositoryImpl
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit

val authenticationModule = module {
    factory { get<Retrofit>().create(AuthenticationService::class.java) }
    factory { AuthenticationRemoteDataSourceImpl(get()) } bind AuthenticationRemoteDataSource::class
    factory { AuthenticationRepositoryImpl(get(), get()) } bind AuthenticationRepository::class

    factory { SignIn(get()) }
    factory { SignInSocial(get()) }
    factory { SignUp(get()) }
    factory { ForgetPassword(get()) }
    factory { GetProfile(get()) }
    factory { GetCurrentLanguage(get()) }
    factory { UpdateCurrentLanguage(get()) }
    factory { UpdatePassword(get()) }
    factory { UpdateName(get()) }


    viewModel { SignUpViewModel(get(), get(), get(), get()) }
    viewModel { SignInViewModel(get(), get(), get(), get(), get()) }
    viewModel { ProfileViewModel(get(), get(), get(), get(), get(), get(), get(), get()) }

}