package com.abosaham.authentication.remote

import com.abosaham.authentication.remote.dto.ForgetPasswordResponse
import com.abosaham.authentication.remote.dto.UpdateNameDto
import com.abosaham.authentication.remote.dto.UpdatePasswordDto
import com.abosaham.authentication.remote.dto.User
import io.reactivex.Completable
import io.reactivex.Single

class AuthenticationRemoteDataSourceImpl(private val service: AuthenticationService) :
    AuthenticationRemoteDataSource {
    override fun updateName(email: String, token: String, name: String): Single<UpdateNameDto> {

        return service.updateName("updateName", email, token, name).map { it.data }
    }

    override fun updatePassword(email: String, token: String, newPassword: String): Single<UpdatePasswordDto> {
        return service.updatePassword("updatePassword", email, token, newPassword).map { it.data }

    }

    override fun signIn(email: String, password: String): Single<User> {
        return service.signIn(email, password).map { it.data }
    }

    override fun forgetPassword(email: String): Single<ForgetPasswordResponse> {
        return service.forgetPassword(email)
    }

    override fun signInSocial(
        name: String,
        email: String,
        userID: String,
        provider: String,
        photoURL: String
    ): Single<User> {
        return service.signInSocial(name, email, userID, provider, photoURL).map { it.data }
    }

    override fun signUp(name: String, email: String, password: String): Completable {
        return service.signUp(name, email, password)
    }
}