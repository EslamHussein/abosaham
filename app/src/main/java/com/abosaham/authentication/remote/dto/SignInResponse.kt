package com.abosaham.authentication.remote.dto

import com.google.gson.annotations.SerializedName

class SignInResponse(@SerializedName("Result") val data: User)