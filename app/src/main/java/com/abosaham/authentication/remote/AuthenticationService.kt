package com.abosaham.authentication.remote

import com.abosaham.authentication.remote.dto.ForgetPasswordResponse
import com.abosaham.authentication.remote.dto.SignInResponse
import com.abosaham.authentication.remote.dto.*
import com.google.gson.annotations.SerializedName
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface AuthenticationService {

    @FormUrlEncoded
    @POST("include/webService.php?do=SignUp")
    fun signUp(
        @Field("name") name: String, @Field("email") email: String, @Field("password") password: String
    ): Completable

    @FormUrlEncoded
    @POST("include/webService.php?do=SignIn")
    fun signIn(
        @Field("email") email: String, @Field("password") password: String
    ): Single<SignInResponse>

    @FormUrlEncoded
    @POST("include/webService.php?do=sendPassword")
    fun forgetPassword(
        @Field("email") email: String
    ): Single<ForgetPasswordResponse>

    @FormUrlEncoded
    @POST("include/webService.php")
    fun updatePassword(
        @Field("do") action:String,
        @Field("username") email: String ,
        @Field("password") token:String,
        @Field("NewPassword") newPassword:String
    ): Single<UpdatePasswordResponse>


    @FormUrlEncoded
    @POST("include/webService.php")
    fun updateName(
        @Field("do") action: String ,
        @Field("username") email: String ,
        @Field("password") token:String,
        @Field("name") newName:String
    ): Single<UpdateNameResponse>



    @FormUrlEncoded
    @POST("include/webService.php?do=SignInFacebookGmail")
    fun signInSocial(
        @Field("name") name: String,
        @Field("email") email: String,
        @Field("userID") userID: String,
        @Field("provider") provider: String,
        @Field("photoURL") photoURL: String
    ): Single<SignInResponse>

}