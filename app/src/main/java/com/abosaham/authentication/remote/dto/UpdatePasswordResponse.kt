package com.abosaham.authentication.remote.dto

import com.google.gson.annotations.SerializedName

class UpdatePasswordResponse(@SerializedName("Result") val data: UpdatePasswordDto)
class UpdatePasswordDto(@SerializedName("password") val password: String)