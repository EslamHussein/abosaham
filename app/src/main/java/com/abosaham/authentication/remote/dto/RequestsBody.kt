package com.abosaham.authentication.remote.dto

data class SignUpSocialBody(
    val name: String,
    val email: String,
    val userID: String,
    val provider: String,
    val photoURL: String
)
