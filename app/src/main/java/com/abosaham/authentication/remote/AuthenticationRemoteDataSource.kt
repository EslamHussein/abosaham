package com.abosaham.authentication.remote

import com.abosaham.authentication.remote.dto.ForgetPasswordResponse
import com.abosaham.authentication.remote.dto.User
import com.abosaham.authentication.remote.dto.*
import io.reactivex.Completable
import io.reactivex.Single

interface AuthenticationRemoteDataSource {
    fun signUp(name: String, email: String, password: String): Completable
    fun signIn(email: String, password: String): Single<User>
    fun forgetPassword(email: String): Single<ForgetPasswordResponse>
    fun signInSocial(
        name: String,
        email: String,
        userID: String,
        provider: String,
        photoURL: String
    ): Single<User>

    fun updatePassword(
        email: String,
        token: String,
        newPassword: String
    ): Single<UpdatePasswordDto>


    fun updateName(
        email: String,
        token: String,
        name: String
    ): Single<UpdateNameDto>
}
