package com.abosaham.authentication.remote.dto

import com.google.gson.annotations.SerializedName

class User(
    @SerializedName("ID") val id: String,
    @SerializedName("name") var name: String,
    @SerializedName("photo") val photo: String?,
    @SerializedName("email") val email: String,
    @SerializedName("phone") val phone: String?,
    @SerializedName("password") var token: String?
)
