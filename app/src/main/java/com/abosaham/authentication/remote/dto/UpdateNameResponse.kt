package com.abosaham.authentication.remote.dto

import com.google.gson.annotations.SerializedName

class UpdateNameResponse(@SerializedName("Result") val data: UpdateNameDto)
class UpdateNameDto(@SerializedName("name") val name: String)