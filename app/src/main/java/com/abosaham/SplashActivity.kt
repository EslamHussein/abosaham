package com.abosaham

import android.os.Bundle
import android.os.Handler
import com.abosaham.authentication.ui.SignUpActivity
import com.abosaham.core.data.pref.PreferenceDataSource
import com.abosaham.news.ui.activities.HomeActivity
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import org.koin.android.ext.android.inject

private const val SPLASH_DISPLAY_LENGTH = 3000L

class SplashActivity : LocalizationActivity() {

    private val prefDataSource: PreferenceDataSource by inject()
    private var handler: Handler = Handler()
    private val runnable = Runnable {
        //        HomeActivity.start(this@SplashActivity)

        if (prefDataSource.getUser() != null)
            HomeActivity.start(this@SplashActivity)
        else
            SignUpActivity.start(this@SplashActivity)

        finish()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        setLanguage(prefDataSource.getCurrentLanguage())

    }

    override fun onStart() {
        super.onStart()
        handler.postDelayed(runnable, SPLASH_DISPLAY_LENGTH)
    }

    override fun onStop() {
        handler.removeCallbacks(runnable)
        super.onStop()
    }
}
