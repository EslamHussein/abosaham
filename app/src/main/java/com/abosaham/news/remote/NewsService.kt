package com.abosaham.news.remote

import com.abosaham.news.remote.dto.GetCategoriesResponse
import com.abosaham.news.remote.dto.GetNewsResponse
import com.abosaham.news.remote.dto.GetRelatedNewsAndCommentsResponse
import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.http.*

interface NewsService {
    @GET("API/{lang}/category/")
    fun getNewsCategories(@Path("lang") lang: String): Single<GetCategoriesResponse>


    @GET("API/{lang}/news/{catId}/")
    fun getNewsByCategory(
        @Path("lang") lang: String, @Path("catId") catId: String,
        @Query("Urgent") urgent: String? = null
    ): Single<GetNewsResponse>

    @GET("include/webService.php?do=RelatedAndComments")
    fun getRelatedNewsAndComments(@Query("newsID") newsId: String): Single<GetRelatedNewsAndCommentsResponse>


    @POST("API/{lang}/comments/")
    fun addComment(
        @Path("lang") lang: String,
        @Field("json_email") email: String,
        @Field("json_password") token: String,
        @Field("do") action: String,
        @Field("comment") comment: String,
        @Field("news") newsId: String
    ): Completable
}