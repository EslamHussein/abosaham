package com.abosaham.news.remote

import com.abosaham.news.remote.dto.Category
import com.abosaham.news.remote.dto.News
import com.abosaham.news.remote.dto.RelatedNewsAndComments
import io.reactivex.Completable
import io.reactivex.Single

class NewsRemoteDataSourceImpl(private val service: NewsService) :
    NewsRemoteDataSource {
    override fun addComment(
        lang: String,
        email: String,
        token: String,
        action: String,
        comment: String,
        newsId: String
    ): Completable {
        return service.addComment(lang, email, token, "insert", comment, newsId)
    }

    override fun getRelatedNewsAndComments(newsId: String): Single<RelatedNewsAndComments> {
        return service.getRelatedNewsAndComments(newsId).map { it.data }
    }

    override fun getNewsByCategory(lang: String, catId: String, urgent: String?): Single<List<News>> {
        return service.getNewsByCategory(lang, catId, urgent).map { it.data }
    }

    override fun getNewsCategories(lang: String): Single<List<Category>> {
        return service.getNewsCategories(lang).map { it.data }
    }
}