package com.abosaham.news.remote.dto

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
class News(
    @SerializedName("ID") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("cat") val catId: String,
    @SerializedName("catName") val catName: String,
    @SerializedName("photo") val photo: String,
    @SerializedName("info") val info: String,
    @SerializedName("add_date") val addedDate: String,
    @SerializedName("add_date_stamp") val addedDateTimeStamp: Long,
    @SerializedName("text") val text: String,
    @SerializedName("Urgent") val urgent: String,
    @SerializedName("visits") val visits: String,
    @SerializedName("comments") val numberOfComments: Int? = 0,
    @SerializedName("images") val images: List<Photo>
) : Parcelable