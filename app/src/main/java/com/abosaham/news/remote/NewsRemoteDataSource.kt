package com.abosaham.news.remote

import com.abosaham.news.remote.dto.Category
import com.abosaham.news.remote.dto.News
import com.abosaham.news.remote.dto.RelatedNewsAndComments
import io.reactivex.Completable
import io.reactivex.Single

interface NewsRemoteDataSource {
    fun getNewsCategories(lang: String): Single<List<Category>>
    fun getNewsByCategory(lang: String, catId: String, urgent: String?): Single<List<News>>
    fun getRelatedNewsAndComments(newsId: String): Single<RelatedNewsAndComments>
    fun addComment(lang: String, email: String, token: String, action: String, comment: String, newsId: String):Completable

}
