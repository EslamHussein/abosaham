package com.abosaham.news.remote.dto

import com.google.gson.annotations.SerializedName

class GetCategoriesResponse(@SerializedName("Result") val data: List<Category>)
class GetNewsResponse(@SerializedName("Result") val data: List<News>)
class GetRelatedNewsAndCommentsResponse(@SerializedName("Result") val data: RelatedNewsAndComments)

class RelatedNewsAndComments(
    @SerializedName("Related") val related: List<News>,
    @SerializedName("Comments") val comments: List<Comment>
)