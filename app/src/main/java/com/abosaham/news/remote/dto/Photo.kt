package com.abosaham.news.remote.dto

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Photo(val photo: String) : Parcelable