package com.abosaham.news.remote.dto

import com.google.gson.annotations.SerializedName

class Comment(@SerializedName("ID")val id: String,@SerializedName("comment") val comment: String,
              @SerializedName("add_by")val addBy: String,@SerializedName("add_date") val addDate: String)