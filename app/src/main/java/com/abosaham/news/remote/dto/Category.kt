package com.abosaham.news.remote.dto

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Category(
    @SerializedName("ID") val id: String,
    @SerializedName("name") val name: String,
    @SerializedName("notes") val notes: String,
    @SerializedName("img") val img: String,
    @SerializedName("add_date") val addDate: String,
    val urgent: String?
) : Parcelable