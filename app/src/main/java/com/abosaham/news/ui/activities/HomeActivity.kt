package com.abosaham.news.ui.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.Observer
import com.abosaham.R
import com.abosaham.authentication.ui.ProfileActivity
import com.abosaham.core.presentation.state.ResourceState
import com.abosaham.news.presentation.HomeViewModel
import com.abosaham.news.remote.dto.Category
import com.abosaham.news.ui.SectionsPagerAdapter
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import org.koin.android.ext.android.inject

class HomeActivity : LocalizationActivity(), NavigationView.OnNavigationItemSelectedListener {
    private val viewModel: HomeViewModel by inject()

    companion object {
        fun start(from: Context) =
            from.startActivity(Intent(from, HomeActivity::class.java))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        navView.setNavigationItemSelectedListener(this)

        viewModel.categories().observe(this, Observer {

            when (it.status) {
                ResourceState.SUCCESS -> {
                    val data = mutableListOf(
                        Category(
                            "0",
                            this@HomeActivity.getString(R.string.menu_urgent_news),
                            "",
                            "",
                            "",
                            "1"
                        )
                    )
                    data.addAll(it.data!!)
                    initTabs(data)

                }
                ResourceState.ERROR -> {

                }
                ResourceState.LOADING -> {

                }
            }
        })
        viewModel.getCategories()

    }

    private fun initTabs(categories: List<Category>) {
        val sectionsPagerAdapter = SectionsPagerAdapter(categories, supportFragmentManager)
        newsCategoriesViewPager.adapter = sectionsPagerAdapter
        newsCategoriesTabs.setupWithViewPager(newsCategoriesViewPager)

    }

    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {

        when (item.itemId) {
            R.id.nav_home -> {

            }
            R.id.nav_notification -> {
                NotificationActivity.start(this)

            }
            R.id.nav_profile -> {
                ProfileActivity.start(this)
            }
            R.id.nav_help -> {

            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }
}
