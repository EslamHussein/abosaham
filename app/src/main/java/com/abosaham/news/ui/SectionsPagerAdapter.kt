package com.abosaham.news.ui

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.abosaham.news.remote.dto.Category
import com.abosaham.news.ui.fragments.NewsListFragment

class SectionsPagerAdapter(private val categories: List<Category>, fm: FragmentManager) :
    FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    override fun getItem(position: Int): Fragment {
        return NewsListFragment.newInstance(categories[position])
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return categories[position].name
    }

    override fun getCount(): Int {
        return categories.size
    }
}