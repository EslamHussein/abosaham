package com.abosaham.news.ui.activities

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.abosaham.R
import com.abosaham.core.presentation.state.ResourceState
import com.abosaham.core.showSnackBar
import com.abosaham.news.presentation.NotificationViewModel
import com.abosaham.news.remote.dto.Category
import com.abosaham.news.remote.dto.News
import com.abosaham.news.ui.NewsAdapter
import com.akexorcist.localizationactivity.ui.LocalizationActivity
import kotlinx.android.synthetic.main.activity_notification.*
import org.koin.android.ext.android.inject

class NotificationActivity : LocalizationActivity(), NewsAdapter.ItemClickListener<News> {


    override fun onItemClickListener(item: News) {

    }

    private val notificationsViewModel: NotificationViewModel by inject()
    private var newsAdapter: NewsAdapter? = null
    private val category = Category("0", "Notification", "", "", "", "1")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)

        setSupportActionBar(notificationToolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)


        newsAdapter = NewsAdapter(onItemClicked = this)
        notificationsViewModel.notifications().observe(this, Observer {
            when (it.status) {
                ResourceState.SUCCESS -> {
                    notificationsRecyclerView.apply {
                        layoutManager = LinearLayoutManager(this@NotificationActivity)
                        adapter = newsAdapter
                    }
                    newsAdapter?.addItems(it.data!!)
                    notificationSwipeToRefresh.isRefreshing = false
                }
                ResourceState.ERROR -> {
                    window.decorView.rootView.showSnackBar(msg = it.message!!, action = View.OnClickListener {
                        notificationsViewModel.getNotifications(category.id, category.urgent)
                    })
                    notificationSwipeToRefresh.isRefreshing = false
                }
                ResourceState.LOADING -> {
                    notificationSwipeToRefresh.isRefreshing = true
                }
            }
        })

        notificationsViewModel.getNotifications(category.id, category.urgent)
        notificationSwipeToRefresh.setOnRefreshListener {
            notificationsViewModel.getNotifications(category.id, category.urgent)
        }

    }


    companion object {
        fun start(from: Context) =
            from.startActivity(Intent(from, NotificationActivity::class.java))
    }
}
