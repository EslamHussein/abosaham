package com.abosaham.news.ui.activities

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.abosaham.R
import com.abosaham.core.presentation.state.ResourceState
import com.abosaham.core.showSnackBar
import com.abosaham.news.presentation.NewsDetailsViewModel
import com.abosaham.news.remote.dto.News
import com.abosaham.news.ui.NewsAdapter
import com.abosaham.news.ui.PhotosSliderAdapter
import kotlinx.android.synthetic.main.activity_news_details.*
import kotlinx.android.synthetic.main.content_news_details.*
import org.koin.android.ext.android.inject


class NewsDetailsActivity : AppCompatActivity(), NewsAdapter.ItemClickListener<News> {
    private val newsDetailsViewModel: NewsDetailsViewModel by inject()

    private var relatedNewsAdapter: NewsAdapter? = null

    private lateinit var news: News

    companion object {
        const val ARG_NEWS = "arg.news"
        fun start(from: Context?, news: News) =
            from?.apply {
                startActivity(Intent(from, NewsDetailsActivity::class.java).apply {
                    putExtra(ARG_NEWS, news)
                })
            }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_details)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        title = ""

        news = intent.getParcelableExtra(ARG_NEWS)


        numberOfCommentsDateTextView.text = "${news.numberOfComments}"
        numberOfViewsDateTextView.text = news.visits
        newsDateTextView.text = news.addedDate
        newsCategoryNameTextView.text = news.catName
        newsTitleTextView.text = news.name

        newsContentTextView.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(news.text, Html.FROM_HTML_MODE_COMPACT)
        } else {
            Html.fromHtml(news.text)
        }



        newsImagesViewPager.apply {
            adapter = PhotosSliderAdapter(news.images)
        }
        newsImagesIndicator.setViewPager(newsImagesViewPager)


        relatedNewsAdapter = NewsAdapter(onItemClicked = this)


        relatedNewsRecyclerView.apply {
            layoutManager = LinearLayoutManager(this@NewsDetailsActivity)
            adapter = relatedNewsAdapter
        }


        newsDetailsViewModel.relatedNewsAndComments().observe(this, Observer {
            when (it.status) {
                ResourceState.SUCCESS -> {

                    relatedNewsAdapter?.addItems(it.data?.related ?: emptyList())
//                    notificationSwipeToRefresh.isRefreshing = false
                }
                ResourceState.ERROR -> {
                    window.decorView.rootView.showSnackBar(msg = it.message!!, action = View.OnClickListener {
                        newsDetailsViewModel.getRelatedNewsAndComments(news.id)
                    })
//                    notificationSwipeToRefresh.isRefreshing = false
                }
                ResourceState.LOADING -> {
//                    notificationSwipeToRefresh.isRefreshing = true
                }
            }
        })

        newsDetailsViewModel.getRelatedNewsAndComments(news.id)


    }

    override fun onItemClickListener(item: News) {
        start(this, news)
    }
}
