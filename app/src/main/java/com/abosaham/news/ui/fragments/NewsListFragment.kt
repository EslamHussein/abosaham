package com.abosaham.news.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.abosaham.R
import com.abosaham.core.presentation.state.ResourceState
import com.abosaham.core.showSnackBar
import com.abosaham.news.presentation.NewsListViewModel
import com.abosaham.news.remote.dto.Category
import com.abosaham.news.remote.dto.News
import com.abosaham.news.ui.NewsAdapter
import com.abosaham.news.ui.NewsWithSliderAdapter
import com.abosaham.news.ui.activities.NewsDetailsActivity
import kotlinx.android.synthetic.main.fragment_news_list.*
import org.koin.android.ext.android.inject

class NewsListFragment : Fragment(), NewsAdapter.ItemClickListener<News> {
    override fun onItemClickListener(item: News) {
        NewsDetailsActivity.start(context, item)
    }

    private val newsListViewModel: NewsListViewModel by inject()
    private lateinit var category: Category
    private var newsAdapter: NewsWithSliderAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        category = arguments?.getParcelable<Category>(ARG_CATEGORY)
            ?: throw IllegalArgumentException("You should pass Category object")
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_news_list, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        newsAdapter = NewsWithSliderAdapter(onItemClicked = this, category = category)
        newsListViewModel.news().observe(this, Observer {
            when (it.status) {
                ResourceState.SUCCESS -> {
                    newsRecyclerView.apply {
                        layoutManager = LinearLayoutManager(context)
                        adapter = newsAdapter
                    }
                    newsAdapter?.addItems(it.data!!)
                    newsSwipeToRefresh.isRefreshing = false
                }
                ResourceState.ERROR -> {
                    getView()?.showSnackBar(it.message!!, action = View.OnClickListener {
                        newsListViewModel.getNewsByCategoryId(category.id, category.urgent)
                    })
                    newsSwipeToRefresh.isRefreshing = false
                }
                ResourceState.LOADING -> {
                    newsSwipeToRefresh.isRefreshing = true
                }
            }
        })

        newsListViewModel.getNewsByCategoryId(category.id, category.urgent)
        newsSwipeToRefresh.setOnRefreshListener {
            newsListViewModel.getNewsByCategoryId(category.id, category.urgent)
        }
    }

    companion object {
        private const val ARG_CATEGORY = "CATEGORY"
        fun newInstance(category: Category): NewsListFragment {
            return NewsListFragment().apply {
                arguments = Bundle().apply {
                    putParcelable(ARG_CATEGORY, category)
                }
            }
        }
    }
}