package com.abosaham.news.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.abosaham.R
import com.abosaham.news.remote.dto.Category
import com.abosaham.news.remote.dto.News
import kotlinx.android.synthetic.main.slide_news_item.view.*

class NewsWithSliderAdapter(
    var data: ArrayList<News> = arrayListOf(),
    val category: Category,
    private var onItemClicked: NewsAdapter.ItemClickListener<News>?
) : RecyclerView.Adapter<NewsWithSliderAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val itemView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.slide_news_item, parent, false)
        return ViewHolder(itemView)

    }

    override fun getItemCount(): Int {
        return data.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val current = data[position]
        holder.title.text = data[position].name
        holder.newsImagesViewPager.apply {
            adapter = PhotosSliderAdapter(current.images)
        }
        holder.newsImagesIndicator.setViewPager(holder.newsImagesViewPager)
        holder.newsCategoryNameTextView.text = current.catName


        holder.numberOfCommentsDateTextView.text = "${current.numberOfComments}"
        holder.numberOfViewsDateTextView.text = current.visits
        holder.newsDateTextView.text = current.addedDate

        holder.itemView.setOnClickListener {
            onItemClicked?.onItemClickListener(current)
        }
    }


    fun addItems(newData: List<News>) {
        val startPos = data.size
        data.addAll(newData)
        notifyItemRangeInserted(startPos, newData.size)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title: TextView = view.newsTitleTextView
        val newsImagesViewPager: ViewPager = view.newsImagesViewPager
        val newsImagesIndicator = view.newsImagesIndicator
        val newsCategoryNameTextView: TextView = view.newsCategoryNameTextView
        val numberOfCommentsDateTextView: TextView = view.numberOfCommentsDateTextView
        val numberOfViewsDateTextView: TextView = view.numberOfViewsDateTextView
        val newsDateTextView: TextView = view.newsDateTextView

    }


}