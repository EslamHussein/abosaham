package com.abosaham.news.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.abosaham.R
import com.abosaham.core.loadUrl
import com.abosaham.news.remote.dto.News
import kotlinx.android.synthetic.main.news_item.view.*

class NewsAdapter(
    var data: ArrayList<News> = arrayListOf(), private var onItemClicked: ItemClickListener<News>?
) : RecyclerView.Adapter<NewsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val itemView = LayoutInflater
            .from(parent.context)
            .inflate(R.layout.news_item, parent, false)
        return ViewHolder(itemView)

    }

    override fun getItemCount(): Int {
        return data.count()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val current = data[position]
        holder.title.text = data[position].name
        holder.newsCategoryNameTextView.text = current.catName
        holder.numberOfCommentsDateTextView.text = "${current.numberOfComments}"
        holder.numberOfViewsDateTextView.text = current.visits
        holder.newsDateTextView.text = current.addedDate
        holder.newsImageView.loadUrl(current.photo)
    }


    fun addItems(newData: List<News>) {
        val startPos = data.size
        data.addAll(newData)
        notifyItemRangeInserted(startPos, newData.size)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title: TextView = view.newsTitleTextView
        val newsCategoryNameTextView: TextView = view.newsCategoryNameTextView
        val numberOfCommentsDateTextView: TextView = view.numberOfCommentsDateTextView
        val numberOfViewsDateTextView: TextView = view.numberOfViewsDateTextView
        val newsDateTextView: TextView = view.newsDateTextView
        val newsImageView: ImageView = view.newsImageView
    }

    interface ItemClickListener<Item> {
        fun onItemClickListener(item: Item)
    }

}