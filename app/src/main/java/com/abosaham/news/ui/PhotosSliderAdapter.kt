package com.abosaham.news.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.viewpager.widget.PagerAdapter
import com.abosaham.R
import com.abosaham.core.loadUrl
import com.abosaham.news.remote.dto.Photo

class PhotosSliderAdapter(private val photos: List<Photo> = emptyList()) : PagerAdapter() {

    /*
    This callback is responsible for creating a page. We inflate the layout and set the drawable
    to the ImageView based on the position. In the end we add the inflated layout to the parent
    container .This method returns an object key to identify the page view, but in this example page view
    itself acts as the object key
    */
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val view = LayoutInflater.from(container.context).inflate(R.layout.slider_image_view_item, null)
        val imageView = view.findViewById<ImageView>(R.id.newsImageView)
        imageView.loadUrl(photos[position].photo)
        container.addView(view)
        return view
    }

    /*
    This callback is responsible for destroying a page. Since we are using view only as the
    object key we just directly remove the view from parent container
    */
    override fun destroyItem(container: ViewGroup, position: Int, view: Any) {
        container.removeView(view as View)
    }

    /*
    Returns the count of the total pages
    */
    override fun getCount(): Int {
        return photos.size
    }

    /*
    Used to determine whether the page view is associated with object key returned by instantiateItem.
    Since here view only is the key we return view==object
    */
    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return `object` === view
    }

}