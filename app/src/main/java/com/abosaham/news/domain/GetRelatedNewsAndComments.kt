package com.abosaham.news.domain

import com.abosaham.authentication.domain.SignUp
import com.abosaham.core.domain.UseCase
import com.abosaham.news.remote.dto.RelatedNewsAndComments
import io.reactivex.Single

class GetRelatedNewsAndComments(private val newsRepository: NewsRepository) :
    UseCase<GetRelatedNewsAndComments.Params, Single<RelatedNewsAndComments>> {
    override fun execute(param: Params?): Single<RelatedNewsAndComments> {
        if (param == null) throw IllegalArgumentException("${SignUp.Params::class.java} should be not null.")
        return newsRepository.getRelatedNewsAndComments(param)
    }

    class Params(val newsId: String) {
        companion object {
            fun create(newsId: String): Params {
                return Params(newsId)
            }
        }
    }
}