package com.abosaham.news.domain

import com.abosaham.core.domain.UseCase
import com.abosaham.news.remote.dto.Category
import io.reactivex.Single

class GetCategories(private val repository: NewsRepository) : UseCase<Unit, Single<List<Category>>> {
    override fun execute(param: Unit?): Single<List<Category>> {
        return repository.getNewsCategories()
    }
}