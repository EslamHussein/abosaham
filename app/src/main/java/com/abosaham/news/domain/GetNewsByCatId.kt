package com.abosaham.news.domain

import com.abosaham.authentication.domain.SignUp
import com.abosaham.core.domain.UseCase
import com.abosaham.news.remote.dto.News
import io.reactivex.Single

class GetNewsByCatId(private val newsRepository: NewsRepository) : UseCase<GetNewsByCatId.Params, Single<List<News>>> {
    override fun execute(param: Params?): Single<List<News>> {
        if (param == null) throw IllegalArgumentException("${SignUp.Params::class.java} should be not null.")
        return newsRepository.getNewsByCategory(param)
    }

    class Params(val catId: String, val urgent: String?) {
        companion object {
            fun create(catId: String, urgent: String?): Params {
                return Params(catId, urgent)
            }
        }
    }
}