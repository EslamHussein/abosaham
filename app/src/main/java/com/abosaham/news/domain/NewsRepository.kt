package com.abosaham.news.domain

import com.abosaham.news.remote.dto.Category
import com.abosaham.news.remote.dto.News
import com.abosaham.news.remote.dto.RelatedNewsAndComments
import io.reactivex.Single

interface NewsRepository {
    fun getNewsCategories(): Single<List<Category>>
    fun getNewsByCategory(params: GetNewsByCatId.Params): Single<List<News>>
    fun getRelatedNewsAndComments(params: GetRelatedNewsAndComments.Params): Single<RelatedNewsAndComments>

}
