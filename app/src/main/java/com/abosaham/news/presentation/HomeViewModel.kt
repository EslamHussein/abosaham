package com.abosaham.news.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.abosaham.core.exception.ErrorHandler
import com.abosaham.core.executor.ExecutionThread
import com.abosaham.core.presentation.state.Resource
import com.abosaham.core.presentation.state.ResourceState
import com.abosaham.news.domain.GetCategories
import com.abosaham.news.remote.dto.Category
import io.reactivex.disposables.CompositeDisposable

class HomeViewModel(
    private val errorHandler: ErrorHandler, private val executionThread: ExecutionThread,
    private val compositeDisposable: CompositeDisposable,
    private val getCategoriesUseCase: GetCategories
) : ViewModel() {
    private val categoriesLiveData: MutableLiveData<Resource<List<Category>>> by lazy { MutableLiveData<Resource<List<Category>>>() }


    fun categories(): LiveData<Resource<List<Category>>> {
        return categoriesLiveData
    }

    fun getCategories() {
        compositeDisposable.add(
            getCategoriesUseCase.execute()
                .subscribeOn(executionThread.subscribeScheduler)
                .observeOn(executionThread.observerScheduler)
                .doOnSubscribe {
                    categoriesLiveData.postValue(Resource(ResourceState.LOADING, null, null))
                }
                .subscribe({ response ->
                    categoriesLiveData.postValue(Resource(ResourceState.SUCCESS, response, null))
                }, { throwable ->
                    categoriesLiveData.postValue(
                        Resource(
                            ResourceState.ERROR,
                            null,
                            errorHandler.getErrorMessage(throwable)
                        )
                    )
                })
        )

    }

    override fun onCleared() {
        if (!compositeDisposable.isDisposed) compositeDisposable.dispose()
        super.onCleared()
    }
}
