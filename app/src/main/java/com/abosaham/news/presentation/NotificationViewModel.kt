package com.abosaham.news.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.abosaham.core.exception.ErrorHandler
import com.abosaham.core.executor.ExecutionThread
import com.abosaham.core.presentation.state.Resource
import com.abosaham.core.presentation.state.ResourceState
import com.abosaham.news.domain.GetNewsByCatId
import com.abosaham.news.remote.dto.News
import io.reactivex.disposables.CompositeDisposable

class NotificationViewModel(
    private val errorHandler: ErrorHandler, private val executionThread: ExecutionThread,
    private val compositeDisposable: CompositeDisposable,
    private val getNewsByCatIdUseCase: GetNewsByCatId
) : ViewModel() {
    private val newsLiveData: MutableLiveData<Resource<List<News>>> by lazy { MutableLiveData<Resource<List<News>>>() }


    fun notifications(): LiveData<Resource<List<News>>> {
        return newsLiveData
    }

    fun getNotifications(catId: String, urgent: String?) {
        compositeDisposable.add(
            getNewsByCatIdUseCase.execute(GetNewsByCatId.Params.create(catId, urgent))
                .subscribeOn(executionThread.subscribeScheduler)
                .observeOn(executionThread.observerScheduler)
                .doOnSubscribe {
                    newsLiveData.postValue(Resource(ResourceState.LOADING, null, null))
                }
                .subscribe({ response ->
                    newsLiveData.postValue(Resource(ResourceState.SUCCESS, response, null))
                }, { throwable ->
                    newsLiveData.postValue(
                        Resource(
                            ResourceState.ERROR,
                            null,
                            errorHandler.getErrorMessage(throwable)
                        )
                    )
                })
        )
    }

    override fun onCleared() {
        if (!compositeDisposable.isDisposed) compositeDisposable.dispose()
        super.onCleared()
    }
}