package com.abosaham.news.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.abosaham.core.exception.ErrorHandler
import com.abosaham.core.executor.ExecutionThread
import com.abosaham.core.presentation.state.Resource
import com.abosaham.core.presentation.state.ResourceState
import com.abosaham.news.domain.GetRelatedNewsAndComments
import com.abosaham.news.remote.dto.RelatedNewsAndComments
import io.reactivex.disposables.CompositeDisposable

class NewsDetailsViewModel(
    private val errorHandler: ErrorHandler, private val executionThread: ExecutionThread,
    private val compositeDisposable: CompositeDisposable,
    private val getRelatedNewsAndCommentsUseCase: GetRelatedNewsAndComments
) : ViewModel() {
    private val relatedNewsAndCommentsLiveData: MutableLiveData<Resource<RelatedNewsAndComments>> by lazy { MutableLiveData<Resource<RelatedNewsAndComments>>() }


    fun relatedNewsAndComments(): LiveData<Resource<RelatedNewsAndComments>> {
        return relatedNewsAndCommentsLiveData
    }

    fun getRelatedNewsAndComments(newsId: String) {
        compositeDisposable.add(
            getRelatedNewsAndCommentsUseCase.execute(GetRelatedNewsAndComments.Params.create(newsId))
                .subscribeOn(executionThread.subscribeScheduler)
                .observeOn(executionThread.observerScheduler)
                .doOnSubscribe {
                    relatedNewsAndCommentsLiveData.postValue(Resource(ResourceState.LOADING, null, null))
                }
                .subscribe({ response ->
                    relatedNewsAndCommentsLiveData.postValue(Resource(ResourceState.SUCCESS, response, null))
                }, { throwable ->
                    relatedNewsAndCommentsLiveData.postValue(
                        Resource(
                            ResourceState.ERROR,
                            null,
                            errorHandler.getErrorMessage(throwable)
                        )
                    )
                })
        )

    }

    override fun onCleared() {
        if (!compositeDisposable.isDisposed) compositeDisposable.dispose()
        super.onCleared()
    }
}
