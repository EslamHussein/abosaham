package com.abosaham.news.repository

import com.abosaham.core.data.pref.PreferenceDataSource
import com.abosaham.news.domain.GetNewsByCatId
import com.abosaham.news.domain.GetRelatedNewsAndComments
import com.abosaham.news.domain.NewsRepository
import com.abosaham.news.remote.NewsRemoteDataSource
import com.abosaham.news.remote.dto.Category
import com.abosaham.news.remote.dto.News
import com.abosaham.news.remote.dto.RelatedNewsAndComments
import io.reactivex.Single

class NewsRepositoryImpl(
    private val remoteDataSource: NewsRemoteDataSource, private val preferenceDataSource: PreferenceDataSource
) : NewsRepository {
    override fun getRelatedNewsAndComments(params: GetRelatedNewsAndComments.Params): Single<RelatedNewsAndComments> {
        return remoteDataSource.getRelatedNewsAndComments(params.newsId)
    }

    override fun getNewsByCategory(params: GetNewsByCatId.Params): Single<List<News>> {
        return remoteDataSource.getNewsByCategory(
            preferenceDataSource.getCurrentLanguage(),
            params.catId,
            params.urgent
        )
    }

    override fun getNewsCategories(): Single<List<Category>> {
        return remoteDataSource.getNewsCategories(preferenceDataSource.getCurrentLanguage())
    }
}