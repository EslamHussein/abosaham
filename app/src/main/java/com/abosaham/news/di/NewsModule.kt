package com.abosaham.news.di

import com.abosaham.news.domain.GetCategories
import com.abosaham.news.domain.GetNewsByCatId
import com.abosaham.news.domain.GetRelatedNewsAndComments
import com.abosaham.news.domain.NewsRepository
import com.abosaham.news.presentation.HomeViewModel
import com.abosaham.news.presentation.NewsDetailsViewModel
import com.abosaham.news.remote.NewsRemoteDataSource
import com.abosaham.news.remote.NewsRemoteDataSourceImpl
import com.abosaham.news.remote.NewsService
import com.abosaham.news.repository.NewsRepositoryImpl
import com.abosaham.news.presentation.NewsListViewModel
import com.abosaham.news.presentation.NotificationViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.bind
import org.koin.dsl.module
import retrofit2.Retrofit

val newsModule = module {
    factory { get<Retrofit>().create(NewsService::class.java) }
    factory { NewsRemoteDataSourceImpl(get()) } bind NewsRemoteDataSource::class
    factory { NewsRepositoryImpl(get(), get()) } bind NewsRepository::class

    factory { GetCategories(get()) }
    factory { GetNewsByCatId(get()) }
    factory { GetRelatedNewsAndComments(get()) }

    viewModel { HomeViewModel(get(), get(), get(), get()) }
    viewModel { NewsListViewModel(get(), get(), get(), get()) }
    viewModel { NotificationViewModel(get(), get(), get(), get()) }
    viewModel { NewsDetailsViewModel(get(), get(), get(), get()) }
}